# Berlin Clock
This project takes an input time in 24 hour format and coverts it to a textual representation of a berlin clock.
the project can be ran on iPhone in portrait mode.
The application supports iOS 13 and above.
## How to run the project
1. Download and extract the project from gitlab.
1. Open the file titled "Berlin Clock.xcodeproj" in Xcode.
1. Select your desired simulator device and click run.
1. Input the time you would like to convert (in 24 hour format) and then click the convert button.
1. The Berlin clock representation will then be displayed.