import UIKit

class Model {
    
    var hour: Int
    var minute: Int
    var second: Int
    
    init(hour: Int, minute: Int, second: Int) {
        self.hour = hour
        self.minute = minute
        self.second = second
    }
}
