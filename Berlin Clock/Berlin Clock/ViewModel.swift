import UIKit

class ViewModel {
    var model: Model!
    
    func setTimeFromUser(hour: Int, minute: Int, second: Int) -> Bool {
        if validTime(hour: hour, minute: minute, second: second) {
//            update model if input is valid
            model = Model(hour: hour, minute: minute, second: second)
            return true
        }
        return false
    }
    
    func validTime(hour: Int, minute: Int, second: Int) -> Bool {
//        check the input time is between valid values for a 24 hour clock
        if hour > 23 || hour < 0 {
            return false
        }
        if minute > 59 || minute < 0 {
            return false
        }
        if second > 59 || second < 0 {
            return false
        }
        return true
    }
//    convert input values to berlin clock ("R" = red "Y" = yellow and "0" = off)
    func convertSeconds() -> String {
        if model.second % 2 == 0 {
            return "R\n"
        }
        return "0\n"
    }
    
    func convertHours() -> String {
        var hourText = ""
//        first row of hours
        switch model.hour / 5 {
        case 0:
            hourText = "0000"
        case 1:
            hourText = "R000"
        case 2:
            hourText = "RR00"
        case 3:
            hourText = "RRR0"
        case 4:
            hourText = "RRRR"
        default:
            hourText = "0000"
        }
        hourText += "\n"
//        second row of hours
        switch model.hour % 5 {
        case 0:
            hourText += "0000"
        case 1:
            hourText += "R000"
        case 2:
            hourText += "RR00"
        case 3:
            hourText += "RRR0"
        case 4:
            hourText += "RRRR"
        default:
            hourText += "0000"
        }
        hourText += "\n"
        return hourText
    }
    
    func convertMinutes() -> String {
//        first row of minutes
        let numberOfFiveMins = model.minute / 5
        var minuteText = ""
        
        for index in 1...11 {
            if index > numberOfFiveMins {
//                show off for blocks that arent needed
                minuteText += "0"
            } else if index % 3 == 0 {
//                show red for every third block
                minuteText += "R"
            } else {
//                show yellow if on otherwise
                minuteText += "Y"
            }
        }
        minuteText += "\n"
//        second row of minutes
        switch model.minute % 5 {
        case 0:
            minuteText += "0000"
        case 1:
            minuteText += "Y000"
        case 2:
            minuteText += "YY00"
        case 3:
            minuteText += "YYY0"
        case 4:
            minuteText += "YYYY"
        default:
            minuteText += "0000"
        }
        return minuteText
    }
    
    
}
