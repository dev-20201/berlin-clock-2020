import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var hourTextField: UITextField!
    @IBOutlet var minuteTextField: UITextField!
    @IBOutlet var secondTextField: UITextField!
    @IBOutlet var label: UILabel!
    
    var viewModel: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ViewModel()
    }
    
    @IBAction func convertTime(_ sender: Any) {
        if getTimeFromUser() {
            let convertedText = viewModel.convertSeconds() + viewModel.convertHours() + viewModel.convertMinutes()
            label.text = convertedText
        } else {
            let alert = UIAlertController(title: "Invalid Time Entered", message: "Please enter a valid time.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getTimeFromUser() -> Bool {
        //        get input time and store in model
        guard let hour = Int(hourTextField.text ?? "-1"),
            let minute = Int(minuteTextField.text ?? "-1"),
            let second = Int(secondTextField.text ?? "-1") else {
                return false
        }
        if viewModel.setTimeFromUser(hour: hour, minute: minute, second: second) {
            return true
        }
        return false
    }
    
}

