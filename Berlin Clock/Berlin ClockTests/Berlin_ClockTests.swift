import XCTest
@testable import Berlin_Clock

class Berlin_ClockTests: XCTestCase {
    
    var testVC: ViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        testVC = ViewController()
        testVC.hourTextField = UITextField()
        testVC.minuteTextField = UITextField()
        testVC.secondTextField = UITextField()
        testVC.label = UILabel()
        testVC.viewModel = ViewModel()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        testVC.viewModel = nil
        testVC = nil
        super.tearDown()
    }
    
    func testGetTimeFromUser() {
        testVC.hourTextField.text = "2"
        testVC.minuteTextField.text = "1"
        testVC.secondTextField.text = "5"
        XCTAssertTrue(testVC.getTimeFromUser())
    }


    func testConvertTimePass() {
        testVC.hourTextField.text = "2"
        testVC.minuteTextField.text = "1"
        testVC.secondTextField.text = "5"
        testVC.convertTime(self)
        XCTAssertNotNil(testVC.label.text)
    }
    
    func testConvertTimeFailure() {
        testVC.hourTextField.text = "26"
        testVC.minuteTextField.text = "-1"
        testVC.secondTextField.text = "512"
        testVC.convertTime(self)
        XCTAssertNil(testVC.label.text)
    }
    
}
