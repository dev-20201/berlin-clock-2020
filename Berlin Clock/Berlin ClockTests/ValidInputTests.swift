import XCTest
@testable import Berlin_Clock

class ValidInputTests: XCTestCase {
    
    var sut: ViewModel!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        sut = ViewModel()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        super.tearDown()
    }
    
    func testValidTime() {
        XCTAssertTrue(sut.validTime(hour: 12, minute: 10, second: 5))
    }
    
    func testInvalidHour() {
        XCTAssertFalse(sut.validTime(hour: 30, minute: 10, second: 5))
    }
    func testInvalidMinute() {
        XCTAssertFalse(sut.validTime(hour: 120, minute: 77, second: 5))
    }
    func testInvalidSecond() {
        XCTAssertFalse(sut.validTime(hour: 12, minute: 10, second: -42))
    }
}
