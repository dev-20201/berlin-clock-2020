import XCTest
@testable import Berlin_Clock

class ConversionTests: XCTestCase {

    var sut: ViewModel!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        sut = ViewModel()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        super.tearDown()
    }
    
//    seconds tests
    func testConvertEvenSeconds() {
        sut.setTimeFromUser(hour: 5, minute: 5, second: 8)
        let seconds = sut.convertSeconds()
        XCTAssertEqual(seconds, "R\n")
    }

    func testConvertOddSeconds() {
        sut.setTimeFromUser(hour: 5, minute: 5, second: 7)
        let seconds = sut.convertSeconds()
        XCTAssertEqual(seconds, "0\n")
    }
    
//    hours tests
    func testConvertHoursInSecondRow() {
        sut.setTimeFromUser(hour: 4, minute: 5, second: 7)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "0000\nRRRR\n")
    }
    
    func testConvertHoursInFirstRow() {
        sut.setTimeFromUser(hour: 15, minute: 5, second: 7)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "RRR0\n0000\n")
    }
    
    func testConvertHoursInBothRows() {
        sut.setTimeFromUser(hour: 19, minute: 5, second: 7)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "RRR0\nRRRR\n")
    }
    
    func testConvertHoursMidnight() {
        sut.setTimeFromUser(hour: 0, minute: 0, second: 0)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "0000\n0000\n")
    }
    
    func testConvertHoursElevenFiftyNineFiftyNinePm() {
        sut.setTimeFromUser(hour: 23, minute: 59, second: 59)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "RRRR\nRRR0\n")
    }
    
    func testConvertHoursTwoZeroFourAm() {
        sut.setTimeFromUser(hour: 2, minute: 4, second: 0)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "0000\nRR00\n")
    }
    
    func testConvertHoursEightTwentyThreeAm() {
        sut.setTimeFromUser(hour: 8, minute: 23, second: 0)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "R000\nRRR0\n")
    }
    
    func testConvertHoursFourThirtyFivePm() {
        sut.setTimeFromUser(hour: 16, minute: 35, second: 0)
        let hour = sut.convertHours()
        XCTAssertEqual(hour, "RRR0\nR000\n")
    }
    
//    minutes tests
    func testConvertMinutesInTopRow() {
        sut.setTimeFromUser(hour: 15, minute: 15, second: 7)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute,"YYR00000000\n0000")
    }
    
    func testConvertMinutesInBottomRow() {
        sut.setTimeFromUser(hour: 15, minute: 1, second: 7)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute,"00000000000\nY000")
    }
    
    func testConvertMinutesInBothRows() {
        sut.setTimeFromUser(hour: 15, minute: 16, second: 7)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute,"YYR00000000\nY000")
    }
    
    func testConvertMinutesMidnight() {
        sut.setTimeFromUser(hour: 0, minute: 0, second: 0)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute, "00000000000\n0000")
    }
    
    func testConvertMinutesElevenFiftyNineFiftyNinePm() {
        sut.setTimeFromUser(hour: 23, minute: 59, second: 59)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute, "YYRYYRYYRYY\nYYYY")
    }
    
    func testConvertMinutesTwelveTwentyThreePm() {
        sut.setTimeFromUser(hour: 12, minute: 23, second: 0)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute, "YYRY0000000\nYYY0")
    }
    
    func testConvertMinutesTwelveThirtyFourPm() {
        sut.setTimeFromUser(hour: 12, minute: 34, second: 0)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute, "YYRYYR00000\nYYYY")
    }
    
    func testConvertMinutesTwelveThirtyFivePm() {
        sut.setTimeFromUser(hour: 12, minute: 35, second: 0)
        let minute = sut.convertMinutes()
        XCTAssertEqual(minute, "YYRYYRY0000\n0000")
    }
    
}
